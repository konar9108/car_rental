import './App.css';
import React from "react";
import Header from "../components/header/Header";

import { withNamespaces } from 'react-i18next';
const App=()=> {
    return (
        <div className="App">
            <Header></Header>
        </div>
    );
}

export default withNamespaces()(App);
