
import './Header.css';

import React from "react";


import { Link } from "react-router-dom";
import polandFlag from "../../assets/appImages/poland-flag.png"
import englandFlag from "../../assets/appImages/england-flag.jpg"
import i18n from "../../i18n/i18n"

const Header = ()=> {

    const changeLanguage = (lng) => {
        i18n.changeLanguage(lng).then(()=>console.log("zmiana")).catch(()=>console.log("nie zmiana"));
        console.log(i18n.language)
    }
    return(
    // <Navbar bg="light" expand="lg">
        <div className={"container"}>
            <div className={"header"}>
               <Link className={"headerLogo headerItem"} to="/">Fake Rental Taxi</Link>
                <Link className={"headerItem"} to="/login">{i18n.t('Welcome to React')}</Link>
                <img onClick={()=>changeLanguage("en")} className={"flag"} src={englandFlag} alt={"england"}/>
                <img onClick={()=>changeLanguage("pl")} className={"flag"} src={polandFlag} alt={"poland"}/>

            </div>
        </div>

    // </Navbar>
    )
}

export default Header;
