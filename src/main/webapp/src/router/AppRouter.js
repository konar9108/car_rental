// import tank from './assets/tank.png';
import '../app/App.css';
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";


import React from "react";
import App from "../app/App";
import Login from "../components/login/Login";

function AppRouter() {
  return (
      <div>
          <Router>
              <Switch>
                  <Route path="/" component={App} exact />
                  <Route path="/login" component={Login} exact />
              </Switch>
          </Router>
      </div>
  );
}

export default AppRouter;
