import i18n from "i18next";
import { reactI18nextModule } from "react-i18next";

import translationEN from '../i18n/en/test';
import translationPL from '../i18n/pl/test';

// the translations
const resources = {
    en: {
        translation: translationEN
    },
    pl: {
        translation: translationPL
    }
};
console.log("befor einit");

i18n
    .use(reactI18nextModule)
    .init({
        resources,
        lng: "en",
        keySeparator: false,
        interpolation: {
            escapeValue: false
        }
    });


export default i18n;