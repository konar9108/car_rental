package com.carrental.dto;

import com.carrental.enums.BodyType;
import com.carrental.enums.FuelType;

public class CarFilterDto {

    private String brand;

    private String model;

    private BodyType bodyType;

    private Integer numberOfSeats;

    private FuelType fuelType;

    private boolean isManual;

    private boolean hasAc;

    private Integer minPrice;

    private Integer maxPrice;

}
