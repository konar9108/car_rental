package com.carrental.rest;

import com.carrental.dto.CustomerDto;
import com.carrental.dto.CustomerFindListDto;
import com.carrental.mapper.CustomerFindListMapper;
import com.carrental.mapper.CustomerMapper;
import com.carrental.model.Customer;
import com.carrental.service.CustomerService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/customers")
@CrossOrigin(origins = "http://localhost:3000")
public class CustomerRestController {

    private final CustomerService customerService;

    public CustomerRestController(CustomerService customerService) {
        this.customerService = customerService;
    }

    @PostMapping
    public CustomerFindListDto addCustomer(@RequestBody CustomerFindListDto dto) {
        Customer customer = customerService.save(CustomerFindListMapper.INSTANCE.toEntity(dto));
        return CustomerFindListMapper.INSTANCE.toDto(customer);
    }

    @GetMapping(path = "{customerId}")
    public @ResponseBody
    CustomerDto getCustomerById(@PathVariable("customerId") Long id) {
        return CustomerMapper.INSTANCE.toDto(customerService.findByIdWithReservations(id));
    }

    @GetMapping
    public List<CustomerFindListDto> getAllCustomersOrByFirstNameAndLastName(
            @RequestParam(value = "firstName", required = false) Optional<String> firstName,
            @RequestParam(value = "lastName", required = false) Optional<String> lastName) {
        if(firstName.isPresent() && lastName.isPresent()){
            return customerService.findByFirstNameAndLastName(firstName.get(), lastName.get()).stream()
                    .map(CustomerFindListMapper.INSTANCE::toDto)
                    .collect(Collectors.toList());
        } else {
            return customerService.findAll().stream()
                    .map(CustomerFindListMapper.INSTANCE::toDto)
                    .collect(Collectors.toList());
        }
    }

    @DeleteMapping("/{customerId}")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public void deleteCustomerById(@PathVariable("customerId") Long id) {
        customerService.delete(id);
    }
}
