package com.carrental.rest;

import com.carrental.dto.CarDto;
import com.carrental.mapper.CarMapper;
import com.carrental.model.Car;
import com.carrental.service.CarService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/cars")
@CrossOrigin(origins = "http://localhost:3000")
public class CarRestController {

    private final CarService carService;

    public CarRestController(CarService carService) {
        this.carService = carService;
    }

    @PostMapping
    @ResponseStatus(HttpStatus.ACCEPTED)
    public CarDto addCar(@RequestBody CarDto dto, @RequestParam(value = "deptId") Long deptId ) {
            Car car = CarMapper.INSTANCE.toEntity(dto);
            car.addDepartment(deptId);
           car = carService.save(car);
           return CarMapper.INSTANCE.toDto(car);
    }

    @GetMapping
    public List<CarDto> getAllCars() {
        return carService.findAll().stream().map(CarMapper.INSTANCE::toDto).collect(Collectors.toList());
    }

    @GetMapping("/{carId}")
    public @ResponseBody
    CarDto getCarById(@PathVariable("carId") Long id) {
        return CarMapper.INSTANCE.toDto(carService.find(id));
    }

    @DeleteMapping("/{carId}")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public void deleteCarById(@PathVariable("carId") Long id) {
        carService.delete(id);
    }
}
