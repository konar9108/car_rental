package com.carrental.rest;

import com.carrental.dto.ReservationDto;
import com.carrental.dto.ReservationFindListDto;
import com.carrental.mapper.ReservationFindListMapper;
import com.carrental.mapper.ReservationMapper;
import com.carrental.model.Reservation;
import com.carrental.service.ReservationService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/reservations")
@CrossOrigin(origins = "http://localhost:3000")
public class ReservationRestController {

    private final ReservationService reservationService;

    public ReservationRestController(ReservationService reservationService) {
        this.reservationService = reservationService;
    }

    @PostMapping
    public ReservationFindListDto addReservation(@RequestBody ReservationFindListDto dto) {
        Reservation reservation = reservationService.save(ReservationFindListMapper.INSTANCE.toEntity(dto));
        return ReservationFindListMapper.INSTANCE.toDto(reservation);
    }

    @GetMapping(path = "{reservationId}")
    public @ResponseBody
    ReservationDto getReservationById(@PathVariable("reservationId") Long id) {
        return ReservationMapper.INSTANCE.toDto(reservationService.find(id));
    }

    @GetMapping
    public List<ReservationFindListDto> getAllReservations() {
        return reservationService.findAll().stream()
                .map(ReservationFindListMapper.INSTANCE::toDto)
                .collect(Collectors.toList());
    }

    @DeleteMapping("/{reservationId}")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public void deleteReservationById(@PathVariable("reservationId") Long id) {
        reservationService.delete(id);
    }
}
